HTML_TARGETS=index.html
ALL_HTML_TARGETS=index.html index.full.html index.embed.html

TARGETS=${HTML_TARGETS}

BUILDDIR=public

all: ${TARGETS}

%.html: %.md pandoc/video-filter.py pandoc/revealjs-template.html bib/library.bibtex
	pandoc --citeproc \
	--filter ./pandoc/video-filter.py \
	--mathjax \
	--template=./pandoc/revealjs-template.html \
	-t revealjs \
	--slide-level 2 \
	-V controls=true \
	-V controlsTutorial=true \
	-V slideNumber="'c'" \
	-V history \
	-s $< -o $@

%.embed.html: %.md
	pandoc --citeproc \
	--filter ./pandoc/video-filter.py \
	--mathjax \
	--template=./pandoc/revealjs-template.html \
	-t revealjs \
	-V controls=true \
	-V controlsTutorial=true \
	-V slideNumber=false \
	--slide-level 2 \
	-V history \
	-s $< -o $@

%.full.html: %.md
	pandoc --citeproc \
	--filter ./pandoc/video-filter.py \
	--self-contained \
	--mathjax \
	--template=./pandoc/revealjs-template.html \
	-t revealjs \
	--slide-level 2 \
	-V controls=false \
	-V slideNumber="'c'" \
	-V history \
	-s $< -o $@

build: ${ALL_HTML_TARGETS}
	mkdir -p ${BUILDDIR}
	cp ${ALL_HTML_TARGETS} ${BUILDDIR}/.
	cp -r --parents css ${BUILDDIR}
	cp -r --parents img ${BUILDDIR}
	cp -r --parents js ${BUILDDIR}
	cp -r --parents video ${BUILDDIR}

.PHONY: build

clean:
	rm -f ${TARGETS}
	rm -rf ${BUILDDIR}
