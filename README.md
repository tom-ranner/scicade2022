Slides for my talk at [Scicade2022](https://scicade2021.hi.is/).

Slides available online at:
<https://tom-ranner.gitlab.io/scicade2022/>
